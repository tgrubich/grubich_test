package ru.innopolis.eventshollidays.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import ru.innopolis.eventshollidays.EventsHollidaysApplicationTests;
import ru.innopolis.eventshollidays.model.Client;

import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class ClientDatabaseServicesTest extends EventsHollidaysApplicationTests {

    @Autowired
    private ClientDatabaseServices services;

    private Client testClient;

    @Test
    void testInit() {
        testClient = services.createClient(createTestClient());
    }

    @Test
    void testGetById() {
        Client createdClient = services.createClient(createTestClient());

        Client client = services.getById(createdClient.getId());

        assertNotNull(client);
        assertEquals(client.getId(), createdClient.getId());
        assertEquals(client.getLastName(), createdClient.getLastName());
        assertEquals(client.getFirstName(), createdClient.getFirstName());
        assertEquals(client.getEmail(), createdClient.getEmail());
        assertEquals(client.getPhone(), createdClient.getPhone());
        assertEquals(client.getAddress(), createdClient.getAddress());
        assertEquals(client.getPasswordData(), createdClient.getPasswordData());
    }

    @Test
    void testGetByNonExistingId() {
        UUID uuid = UUID.randomUUID();

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> services.getById(uuid),
                format("Expected to return nothing and throw exception, since object with generated id %s doesn't exist", uuid)
        );

        assertEquals(format("404 NOT_FOUND \"Client with id %s not found\"", uuid), exception.getMessage());
    }


    @Test
    void testGetAll() {
        List<Client> clientsList = services.getAll();
        assertNotNull(clientsList);
    }

    @Test
    void testCreateClient() {
        Client client = createTestClient();

        Client createdClient = services.createClient(client);

        assertNotNull(createdClient.getId());
        assertEquals(client.getLastName(), createdClient.getLastName());
        assertEquals(client.getFirstName(), createdClient.getFirstName());
        assertEquals(client.getEmail(), createdClient.getEmail());
        assertEquals(client.getPhone(), createdClient.getPhone());
        assertEquals(client.getAddress(), createdClient.getAddress());
        assertEquals(client.getPasswordData(), createdClient.getPasswordData());
    }

    @Test
    void testUpdateClient() {
        Client createdClient = services.createClient(createTestClient());

        Client clientToUpdate = new Client(createdClient).toBuilder()
                .id(UUID.randomUUID())
                .lastName("Pavlovsky")
                .firstName("Alexei")
                .email("pavlovsky@gmail.com")
                .phone("+79607778899")
                .address("Novorossiisk")
                .passwordData("passwordNew")
                .build();

        Client updatedClient = services.updateClient(createdClient.getId(), clientToUpdate);

        assertEquals(createdClient.getId(), updatedClient.getId(), "Ids shouldn't be changed during the update");
        assertNotEquals(createdClient.getLastName(), updatedClient.getLastName());
        assertNotEquals(createdClient.getFirstName(), updatedClient.getFirstName());
        assertNotEquals(createdClient.getEmail(), updatedClient.getEmail());
        assertNotEquals(createdClient.getPhone(), updatedClient.getPhone());
        assertNotEquals(createdClient.getAddress(), updatedClient.getAddress());
        assertNotEquals(createdClient.getPasswordData(), updatedClient.getPasswordData());
    }

    @Test
    void testDelete() {
        Client createdClient = services.createClient(createTestClient());
        UUID createdClientId = createdClient.getId();
        services.delete(createdClientId);

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> services.getById(createdClientId),
                format("Expected to return nothing and throw exception, since client with id %s was deleted",
                        createdClient)
        );

        assertEquals(format("404 NOT_FOUND \"Client with id %s not found\"", createdClient.getId()), exception.getMessage());
    }
}