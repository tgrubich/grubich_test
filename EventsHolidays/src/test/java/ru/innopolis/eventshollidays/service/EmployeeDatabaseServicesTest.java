package ru.innopolis.eventshollidays.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import ru.innopolis.eventshollidays.EventsHollidaysApplicationTests;
import ru.innopolis.eventshollidays.model.Employee;

import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeDatabaseServicesTest extends EventsHollidaysApplicationTests {

    @Autowired
    private EmployeeDatabaseServices services;

    private Employee testEmployee;

    @Test
    void testInit() {
        testEmployee = services.createEmployee(createTestEmployee());
    }

    @Test
    void testGetById() {
        Employee createdEmployee = services.createEmployee(createTestEmployee());

        Employee employee = services.getById(createdEmployee.getId());

        assertNotNull(employee);
        assertEquals(employee.getId(), createdEmployee.getId());
        assertEquals(employee.getLastName(), createdEmployee.getLastName());
        assertEquals(employee.getFirstName(), createdEmployee.getFirstName());
        assertEquals(employee.getPosition(), createdEmployee.getPosition());
        assertEquals(employee.getPhone(), createdEmployee.getPhone());
    }

    @Test
    void testGetByNonExistingId() {
        UUID uuid = UUID.randomUUID();

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> services.getById(uuid),
                format("Expected to return nothing and throw exception, since object with generated id %s doesn't exist", uuid)
        );

        assertEquals(format("404 NOT_FOUND \"Employee with id %s not found\"", uuid), exception.getMessage());
    }

    @Test
    void testGetAll() {
        List<Employee> employeesList = services.getAll();
        assertNotNull(employeesList);
    }

    @Test
    void testCreateEmployee() {
        Employee employee = createTestEmployee();

        Employee createdEmployee = services.createEmployee(employee);

        assertNotNull(createdEmployee.getId());
        assertEquals(employee.getLastName(), createdEmployee.getLastName());
        assertEquals(employee.getFirstName(), createdEmployee.getFirstName());
        assertEquals(employee.getPosition(), createdEmployee.getPosition());
        assertEquals(employee.getPhone(), createdEmployee.getPhone());
    }

    @Test
    void testUpdateEmployee() {
        Employee createdEmployee = services.createEmployee(createTestEmployee());

        Employee employeeToUpdate = new Employee(createdEmployee).toBuilder()
                .id(UUID.randomUUID())
                .lastName("Vasechkin")
                .firstName("Nik")
                .position("junior manager")
                .phone("+79607778899")
                .build();

        Employee updatedEmployee = services.updateEmployee(createdEmployee.getId(), employeeToUpdate);

        assertEquals(createdEmployee.getId(), updatedEmployee.getId(), "Ids shouldn't be changed during the update");
        assertNotEquals(createdEmployee.getLastName(), updatedEmployee.getLastName());
        assertNotEquals(createdEmployee.getFirstName(), updatedEmployee.getFirstName());
        assertNotEquals(createdEmployee.getPosition(), updatedEmployee.getPosition());
        assertNotEquals(createdEmployee.getPhone(), updatedEmployee.getPhone());
    }

    @Test
    void testDelete() {
        Employee createdEmployee = services.createEmployee(createTestEmployee());
        UUID createdEmployeeId = createdEmployee.getId();
        services.delete(createdEmployeeId);

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> services.getById(createdEmployeeId),
                format("Expected to return nothing and throw exception, since employee with id %s was deleted",
                        createdEmployee)
        );

        assertEquals(format("404 NOT_FOUND \"Employee with id %s not found\"", createdEmployee.getId()), exception.getMessage());
    }
}