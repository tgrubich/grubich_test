package ru.innopolis.eventshollidays.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import ru.innopolis.eventshollidays.model.Client;
import ru.innopolis.eventshollidays.model.Score;

import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class ScoreDatabaseServicesTest extends EventDatabaseServicesTest{

    @Autowired
    private ScoreDatabaseServices services;

    @Test
    void scoreGetById() {
        Score createdScore = services.createScore(createTestScore());

        Score score = services.getById(createdScore.getId());

        assertNotNull(score);
        assertEquals(score.getId(), createdScore.getId());
        assertEquals(score.getDateCreateScore(), createdScore.getDateCreateScore());
        assertEquals(score.getNumberScore(), createdScore.getNumberScore());
    }

    @Test
    void testGetByNonExistingId() {
        UUID uuid = UUID.randomUUID();

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> services.getById(uuid),
                format("Expected to return nothing and throw exception, since object with generated id %s doesn't exist", uuid)
        );

        assertEquals(format("404 NOT_FOUND \"Score with id %s not found\"", uuid), exception.getMessage());
    }

    @Test
    void scoreGetAll() {
        List<Score> scoresList = services.getAll();
        assertNotNull(scoresList);
    }

    @Test
    void scoreCreateScore() {
        Score score = createTestScore();

        Score createdScore = services.createScore(score);

        assertNotNull(createdScore.getId());
        assertEquals(score.getDateCreateScore(), createdScore.getDateCreateScore());
        assertEquals(score.getNumberScore(), createdScore.getNumberScore());
    }

    @Test
    void scoreUpdateScore() {
        Score createdScore = services.createScore(createTestScore());

        Score scoreToUpdate = new Score(createdScore).toBuilder()
                .id(UUID.randomUUID())
                .dateCreateScore("2021-09-30")
                .numberScore(35)
                .build();

        Score updatedScore = services.updateScore(createdScore.getId(), scoreToUpdate);

        assertEquals(createdScore.getId(), updatedScore.getId(), "Ids shouldn't be changed during the update");
        assertNotEquals(createdScore.getDateCreateScore(), updatedScore.getDateCreateScore());
        assertNotEquals(createdScore.getNumberScore(), updatedScore.getNumberScore());
    }

    @Test
    void scoreDelete() {
        Score createdScore = services.createScore(createTestScore());
        UUID createdScoreId = createdScore.getId();
        services.delete(createdScoreId);

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> services.getById(createdScoreId),
                format("Expected to return nothing and throw exception, since client with id %s was deleted",
                        createdScore)
        );

        assertEquals(format("404 NOT_FOUND \"Score with id %s not found\"", createdScore.getId()), exception.getMessage());
    }
}