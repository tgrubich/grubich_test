package ru.innopolis.eventshollidays.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import ru.innopolis.eventshollidays.EventsHollidaysApplicationTests;
import ru.innopolis.eventshollidays.model.Report;
import ru.innopolis.eventshollidays.model.ReportName;

import java.util.*;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class ReportDatabaseServicesTest extends EventsHollidaysApplicationTests {

    @Autowired
    private ReportDatabaseServices services;

    private Report testReport;

    @Test
    void testInit() {
        testReport = services.createReport(createTestReport());
    }

    @Test
    void testGetById() {
        Report createdReport = services.createReport(createTestReport());

        Report report = services.getById(createdReport.getId());

        assertNotNull(report);
        assertEquals(report.getId(), createdReport.getId());
        assertEquals(report.getDateCreateReport(), createdReport.getDateCreateReport());
        assertEquals(report.getReportName(), createdReport.getReportName());
        assertEquals(report.getReportData(), createdReport.getReportData());
    }

    @Test
    void testGetByNonExistingId() {
        UUID uuid = UUID.randomUUID();

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> services.getById(uuid),
                format("Expected to return nothing and throw exception, since object with generated id %s doesn't exist", uuid)
        );

        assertEquals(format("404 NOT_FOUND \"Report with id %s not found\"", uuid), exception.getMessage());
    }

    @Test
    void testGetAll() {
        List<Report> reportsList = services.getAll();
        assertNotNull(reportsList);
    }

    @Test
    void testCreateReport() {
        Report report = createTestReport();

        Report createdReport = services.createReport(report);

        assertNotNull(createdReport.getId());
        assertEquals(report.getDateCreateReport(), createdReport.getDateCreateReport());
        assertEquals(report.getReportName(), createdReport.getReportName());
        assertEquals(report.getReportData(), createdReport.getReportData());
    }

    @Test
    void testUpdateReport() {
        Report createdReport = services.createReport(createTestReport());

        Report reportToUpdate = new Report(createdReport).toBuilder()
                .id(UUID.randomUUID())
                .dateCreateReport("2021-10-04")
                .reportName(ReportName.BY_FUNDS_RECEIVED)
                .reportData("data 123")
                .build();

        Report updatedReport = services.updateReport(createdReport.getId(), reportToUpdate);

        assertEquals(createdReport.getId(), updatedReport.getId(), "Ids shouldn't be changed during the update");
        assertNotEquals(createdReport.getDateCreateReport(), updatedReport.getDateCreateReport());
        assertNotEquals(createdReport.getReportName(), updatedReport.getReportName());
        assertNotEquals(createdReport.getReportData(), updatedReport.getReportData());
    }

    @Test
    void testDelete() {
        Report createdReport = services.createReport(createTestReport());
        UUID createdReportId = createdReport.getId();
        services.delete(createdReportId);

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> services.getById(createdReportId),
                format("Expected to return nothing and throw exception, since client with id %s was deleted",
                        createdReport)
        );

        assertEquals(format("404 NOT_FOUND \"Report with id %s not found\"", createdReport.getId()), exception.getMessage());
    }
}