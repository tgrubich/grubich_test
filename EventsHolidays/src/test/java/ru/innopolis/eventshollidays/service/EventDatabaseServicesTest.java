package ru.innopolis.eventshollidays.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import ru.innopolis.eventshollidays.EventsHollidaysApplicationTests;
import ru.innopolis.eventshollidays.model.Category;
import ru.innopolis.eventshollidays.model.Event;

import java.util.*;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class EventDatabaseServicesTest extends EventsHollidaysApplicationTests {

    @Autowired
    private EventDatabaseServices eventDatabaseServices;

    private Event testEvent;

    @Test
    void testInit() {
        testEvent = eventDatabaseServices.createEvent(createTestEvent());
    }

    @Test
    void testGetById() {
        Event createdEvent = eventDatabaseServices.createEvent(createTestEvent());

        Event event = eventDatabaseServices.getById(createdEvent.getId());

        assertNotNull(event);
        assertEquals(event.getId(), createdEvent.getId());
        assertEquals(event.getDateEvent(), createdEvent.getDateEvent());
        assertEquals(event.getCategory(), createdEvent.getCategory());
    }

    @Test
    void testGetByNonExistingId() {
        UUID uuid = UUID.randomUUID();

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> eventDatabaseServices.getById(uuid),
                format("Expected to return nothing and throw exception, since object with generated id %s doesn't exist", uuid)
        );

        assertEquals(format("404 NOT_FOUND \"Event with id %s not found\"", uuid), exception.getMessage());
    }

    @Test
    void testGetAll() {
        List<Event> eventsList = eventDatabaseServices.getAll();
        assertNotNull(eventsList);
    }

    @Test
    void testCreateEvent() {
        Event event = createTestEvent();

        Event createdEvent = eventDatabaseServices.createEvent(event);

        assertNotNull(createdEvent.getId());
        assertEquals(event.getDateEvent(), createdEvent.getDateEvent());
        assertEquals(event.getCategory(), createdEvent.getCategory());
    }

    @Test
    void testUpdateEvent() {
        Event createdEvent = eventDatabaseServices.createEvent(createTestEvent());

        Event eventToUpdate = new Event(createdEvent).toBuilder()
                .id(UUID.randomUUID())
                .dateEvent("2021-11-11")
                .category(Category.FOR_CHILDREN)
                .build();

        Event updatedEvent = eventDatabaseServices.updateEvent(createdEvent.getId(), eventToUpdate);

        assertEquals(createdEvent.getId(), updatedEvent.getId(), "Ids shouldn't be changed during the update");
        assertNotEquals(createdEvent.getDateEvent(), updatedEvent.getDateEvent());
        assertNotEquals(createdEvent.getCategory(), updatedEvent.getCategory());
    }

    @Test
    void testDelete() {
        Event createdEvent = eventDatabaseServices.createEvent(createTestEvent());
        UUID createdEventId = createdEvent.getId();
        eventDatabaseServices.delete(createdEventId);

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> eventDatabaseServices.getById(createdEventId),
                format("Expected to return nothing and throw exception, since event with id %s was deleted",
                        createdEvent)
        );

        assertEquals(format("404 NOT_FOUND \"Event with id %s not found\"", createdEvent.getId()), exception.getMessage());
    }
}