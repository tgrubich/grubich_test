package ru.innopolis.eventshollidays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;
import ru.innopolis.eventshollidays.model.*;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class EventsHollidaysApplicationTests {

    @LocalServerPort
    protected int port;

    protected String baseUrl = "http://localhost:";
    protected RestTemplate restTemplate = new RestTemplate();

    @BeforeEach
    public void setUp() {
        baseUrl = baseUrl.concat(port + "");
    }


    @Test
    void contextLoads() {
    }

    protected Client createTestClient() {
        return Client.builder()
                .lastName("Grubich")
                .firstName("Tati")
                .email("tati@gmail.com")
                .phone("+79605556677")
                .address("Kazan")
                .passwordData("password")
                .build();
    }

    protected Employee createTestEmployee() {
        return Employee.builder()
                .lastName("Lucic")
                .firstName("Andrey")
                .position("manager")
                .phone("+79606667788")
                .build();
    }

    protected Event createTestEvent() {
        return Event.builder()
                .dateEvent("2021-12-11")
                .category(Category.FOR_ADULTS)
                .build();
    }

    protected Report createTestReport() {
        return Report.builder()
                .dateCreateReport("2021-12-09")
                .reportName(ReportName.BY_EVENTS)
                .reportData("event`s report")
                .build();
    }

    protected Score createTestScore() {
        return Score.builder()
                .dateCreateScore("2021-12-31")
                .numberScore(31)
                .price(3000)
                .build();
    }
}
