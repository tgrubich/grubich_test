package ru.innopolis.eventshollidays.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.innopolis.eventshollidays.EventsHollidaysApplicationTests;
import ru.innopolis.eventshollidays.model.Client;
import ru.innopolis.eventshollidays.model.Score;
import ru.innopolis.eventshollidays.service.ScoreDatabaseServices;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class ScoreControllerTest extends EventsHollidaysApplicationTests {

    @Autowired
    private ScoreDatabaseServices services;

    @Test
    void getAllScores() {
        ResponseEntity<List<Score>> scoresListResponse = restTemplate.exchange(baseUrl + "/scores",
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });

        List<Score> scoresList = scoresListResponse.getBody();

        assertEquals(HttpStatus.OK, scoresListResponse.getStatusCode(),
                "Status code for the get all clients request should be 200");

        assertNotNull(scoresList);
    }

    @Test
    void getById() {
        Score score = createTestScore();
        UUID uuid = UUID.randomUUID();
        score.setId(uuid);
        services.createScore(score);

        ResponseEntity<Score> scoreResponse = restTemplate.exchange(baseUrl + "/scores/" + score.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Score receivedScore = scoreResponse.getBody();

        assertEquals(HttpStatus.OK, scoreResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedScore);
    }

    @Test
    void createScore() {
        Score score = createTestScore();
        UUID uuid = UUID.randomUUID();
        score.setId(uuid);
        services.createScore(score);

        ResponseEntity<Score> scoreResponse = restTemplate.exchange(baseUrl + "/scores/" + score.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Score receivedScore = scoreResponse.getBody();

        assertEquals(HttpStatus.OK, scoreResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedScore);
    }

    @Test
    void updateScore() {
        Score score = createTestScore();
        services.createScore(score);

        Score updatedScore = new Score();
        updatedScore.setId(score.getId());
        updatedScore.setDateCreateScore("2021-01-01");
        updatedScore.setNumberScore(2);
        updatedScore.setPrice(5000);
        services.updateScore(score.getId(), updatedScore);

        ResponseEntity<Score> scoreResponse = restTemplate.exchange(baseUrl + "/scores/" + updatedScore.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Score receivedScore = scoreResponse.getBody();

        assertEquals(HttpStatus.OK, scoreResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedScore);
    }

    @Test
    void deleteScore() {
        Score score = createTestScore();
        services.createScore(score);

        services.delete(score.getId());

        ResponseEntity<Score> scoreResponse = restTemplate.exchange(baseUrl + "/scores/" + score.getId(),
                HttpMethod.POST, null, new ParameterizedTypeReference<>() {
                });
        Score receivedScore = scoreResponse.getBody();

        assertEquals(HttpStatus.OK, scoreResponse.getStatusCode(),
                "Status code for the get client by id request should be 404");

        assertNull(receivedScore);
    }
}