package ru.innopolis.eventshollidays.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.innopolis.eventshollidays.EventsHollidaysApplicationTests;
import ru.innopolis.eventshollidays.model.Category;
import ru.innopolis.eventshollidays.model.Client;
import ru.innopolis.eventshollidays.model.Event;
import ru.innopolis.eventshollidays.service.EventDatabaseServices;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class EventControllerTest extends EventsHollidaysApplicationTests {

    @Autowired
    private EventDatabaseServices services;

    @Test
    void testGetAllEvents() {
        ResponseEntity<List<Event>> eventsListResponse = restTemplate.exchange(baseUrl + "/events",
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });

        List<Event> eventsList = eventsListResponse.getBody();

        assertEquals(HttpStatus.OK, eventsListResponse.getStatusCode(),
                "Status code for the get all clients request should be 200");

        assertNotNull(eventsList);
    }

    @Test
    void testGetById() {
        Event event = createTestEvent();
        UUID uuid = UUID.randomUUID();
        event.setId(uuid);
        services.createEvent(event);

        ResponseEntity<Event> eventResponse = restTemplate.exchange(baseUrl + "/events/" + event.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Event receivedEvent = eventResponse.getBody();

        assertEquals(HttpStatus.OK, eventResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedEvent);
    }

    @Test
    void testCreateEvent() {
        Event event = createTestEvent();
        UUID uuid = UUID.randomUUID();
        event.setId(uuid);
        services.createEvent(event);

        ResponseEntity<Event> eventResponse = restTemplate.exchange(baseUrl + "/events/" + event.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Event receivedEvent = eventResponse.getBody();

        assertEquals(HttpStatus.OK, eventResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedEvent);
    }

    @Test
    void testUpdateEvent() {
        Event event = createTestEvent();
        services.createEvent(event);

        Event updatedEvent = new Event();
        updatedEvent.setId(event.getId());
        updatedEvent.setDateEvent("2021-12-13");
        updatedEvent.setCategory(Category.FOR_CHILDREN);
        services.updateEvent(event.getId(), updatedEvent);

        ResponseEntity<Event> eventResponse = restTemplate.exchange(baseUrl + "/events/" + updatedEvent.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Event receivedEvent = eventResponse.getBody();

        assertEquals(HttpStatus.OK, eventResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedEvent);
    }

    @Test
    void testDeleteEvent() {
        Event event = createTestEvent();
        services.createEvent(event);

        services.delete(event.getId());

        ResponseEntity<Event> eventResponse = restTemplate.exchange(baseUrl + "/events/" + event.getId(),
                HttpMethod.POST, null, new ParameterizedTypeReference<>() {
                });
        Event receivedEvent = eventResponse.getBody();

        assertEquals(HttpStatus.OK, eventResponse.getStatusCode(),
                "Status code for the get client by id request should be 404");

        assertNull(receivedEvent);
    }
}