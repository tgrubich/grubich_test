package ru.innopolis.eventshollidays.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.innopolis.eventshollidays.EventsHollidaysApplicationTests;
import ru.innopolis.eventshollidays.model.Client;
import ru.innopolis.eventshollidays.model.Employee;
import ru.innopolis.eventshollidays.service.EmployeeDatabaseServices;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeControllerTest extends EventsHollidaysApplicationTests {

    @Autowired
    private EmployeeDatabaseServices services;

    @Test
    void testGetAllEmployees() {
        ResponseEntity<List<Employee>> employeesListResponse = restTemplate.exchange(baseUrl + "/employees",
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });

        List<Employee> employeesList = employeesListResponse.getBody();

        assertEquals(HttpStatus.OK, employeesListResponse.getStatusCode(),
                "Status code for the get all employees request should be 200");

        assertNotNull(employeesList);
    }

    @Test
    void testGetById() {
        Employee employee = createTestEmployee();
        UUID uuid = UUID.randomUUID();
        employee.setId(uuid);
        services.createEmployee(employee);

        ResponseEntity<Employee> employeeResponse = restTemplate.exchange(baseUrl + "/employees/" + employee.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Employee receivedEmployee = employeeResponse.getBody();

        assertEquals(HttpStatus.OK, employeeResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedEmployee);
    }

    @Test
    void testCreateEmployee() {
        Employee employee = createTestEmployee();
        UUID uuid = UUID.randomUUID();
        employee.setId(uuid);
        services.createEmployee(employee);

        ResponseEntity<Employee> employeeResponse = restTemplate.exchange(baseUrl + "/employees/" + employee.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Employee receivedEmployee = employeeResponse.getBody();

        assertEquals(HttpStatus.OK, employeeResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedEmployee);
    }

    @Test
    void testUpdateEmployee() {
        Employee employee = createTestEmployee();
        services.createEmployee(employee);

        Employee updatedEmployee = new Employee();
        updatedEmployee.setId(employee.getId());
        updatedEmployee.setLastName("Polovec");
        updatedEmployee.setFirstName("Ivan");
        updatedEmployee.setPosition("main manager");
        updatedEmployee.setPhone("+79289996633");
        services.updateEmployee(employee.getId(), updatedEmployee);

        ResponseEntity<Employee> employeeResponse = restTemplate.exchange(baseUrl + "/employees/" + updatedEmployee.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Employee receivedEmployee = employeeResponse.getBody();

        assertEquals(HttpStatus.OK, employeeResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedEmployee);
    }

    @Test
    void testDeleteEmployee() {
        Employee employee = createTestEmployee();
        services.createEmployee(employee);

        services.delete(employee.getId());

        ResponseEntity<Employee> employeeResponse = restTemplate.exchange(baseUrl + "/employees/" + employee.getId(),
                HttpMethod.POST, null, new ParameterizedTypeReference<>() {
                });
        Employee receivedEmployee = employeeResponse.getBody();

        assertEquals(HttpStatus.OK, employeeResponse.getStatusCode(),
                "Status code for the get client by id request should be 404");

        assertNull(receivedEmployee);
    }
}