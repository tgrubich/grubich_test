package ru.innopolis.eventshollidays.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.innopolis.eventshollidays.EventsHollidaysApplicationTests;
import ru.innopolis.eventshollidays.model.Client;
import ru.innopolis.eventshollidays.service.ClientDatabaseServices;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class ClientControllerTest extends EventsHollidaysApplicationTests {

    @Autowired
    private ClientDatabaseServices services;

    @Test
    void testGetAllClients() {
        ResponseEntity<List<Client>> clientsListResponse = restTemplate.exchange(baseUrl + "/clients",
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });

        List<Client> clientsList = clientsListResponse.getBody();

        assertEquals(HttpStatus.OK, clientsListResponse.getStatusCode(),
                "Status code for the get all clients request should be 200");

        assertNotNull(clientsList);
    }

    @Test
    void testGetById() {
        Client client = createTestClient();
        UUID uuid = UUID.randomUUID();
        client.setId(uuid);
        services.createClient(client);

        ResponseEntity<Client> clientResponse = restTemplate.exchange(baseUrl + "/clients/" + client.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Client receivedClient = clientResponse.getBody();

        assertEquals(HttpStatus.OK, clientResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedClient);
    }

    @Test
    void testCreateClient() {
        Client client = createTestClient();
        UUID uuid = UUID.randomUUID();
        client.setId(uuid);
        services.createClient(client);

        ResponseEntity<Client> clientResponse = restTemplate.exchange(baseUrl + "/clients/" + client.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Client receivedClient = clientResponse.getBody();

        assertEquals(HttpStatus.OK, clientResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedClient);
    }

    @Test
    void testUpdateClient() {
        Client client = createTestClient();
        services.createClient(client);

        Client updatedClient = new Client();
        updatedClient.setId(client.getId());
        updatedClient.setLastName("Baturdinov");
        updatedClient.setFirstName("Kamil");
        updatedClient.setEmail("kamil@kamil.ru");
        updatedClient.setPhone("+79607778899");
        updatedClient.setAddress("Novosibirsk");
        updatedClient.setPasswordData("password data");
        services.updateClient(client.getId(), updatedClient);

        ResponseEntity<Client> clientResponse = restTemplate.exchange(baseUrl + "/clients/" + updatedClient.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Client receivedClient = clientResponse.getBody();

        assertEquals(HttpStatus.OK, clientResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedClient);
    }

    @Test
    void testDeleteClient() {
        Client client = createTestClient();
        services.createClient(client);

        services.delete(client.getId());

        ResponseEntity<Client> clientResponse = restTemplate.exchange(baseUrl + "/clients/" + client.getId(),
                HttpMethod.POST, null, new ParameterizedTypeReference<>() {
                });
        Client receivedClient = clientResponse.getBody();

        assertEquals(HttpStatus.OK, clientResponse.getStatusCode(),
                "Status code for the get client by id request should be 404");

        assertNull(receivedClient);
    }
}