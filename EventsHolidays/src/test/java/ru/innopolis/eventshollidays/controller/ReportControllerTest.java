package ru.innopolis.eventshollidays.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.innopolis.eventshollidays.EventsHollidaysApplicationTests;
import ru.innopolis.eventshollidays.model.Client;
import ru.innopolis.eventshollidays.model.Report;
import ru.innopolis.eventshollidays.model.ReportName;
import ru.innopolis.eventshollidays.service.ReportDatabaseServices;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class ReportControllerTest extends EventsHollidaysApplicationTests {

    @Autowired
    private ReportDatabaseServices services;

    @Test
    void testGetAllReports() {
        ResponseEntity<List<Report>> reportsListResponse = restTemplate.exchange(baseUrl + "/reports",
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });

        List<Report> reportsList = reportsListResponse.getBody();

        assertEquals(HttpStatus.OK, reportsListResponse.getStatusCode(),
                "Status code for the get all clients request should be 200");

        assertNotNull(reportsList);
    }

    @Test
    void testGetById() {
        Report report = createTestReport();
        UUID uuid = UUID.randomUUID();
        report.setId(uuid);
        services.createReport(report);

        ResponseEntity<Report> reportResponse = restTemplate.exchange(baseUrl + "/reports/" + report.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Report receivedReport = reportResponse.getBody();

        assertEquals(HttpStatus.OK, reportResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedReport);
    }

    @Test
    void testCreateReport() {
        Report report = createTestReport();
        UUID uuid = UUID.randomUUID();
        report.setId(uuid);
        services.createReport(report);

        ResponseEntity<Report> reportResponse = restTemplate.exchange(baseUrl + "/reports/" + report.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Report receivedReport = reportResponse.getBody();

        assertEquals(HttpStatus.OK, reportResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedReport);
    }

    @Test
    void testUpdateReport() {
        Report report = createTestReport();
        services.createReport(report);

        Report updatedReport = new Report();
        updatedReport.setId(report.getId());
        updatedReport.setDateCreateReport("2021-10-04");
        updatedReport.setReportName(ReportName.BY_FUNDS_RECEIVED);
        updatedReport.setReportData("my report");
        services.updateReport(report.getId(), updatedReport);

        ResponseEntity<Report> reportResponse = restTemplate.exchange(baseUrl + "/reports/" + updatedReport.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        Report receivedReport = reportResponse.getBody();

        assertEquals(HttpStatus.OK, reportResponse.getStatusCode(),
                "Status code for the get client by id request should be 200");

        assertNotNull(receivedReport);
    }

    @Test
    void testDeleteReport() {
        Report report = createTestReport();
        services.createReport(report);

        services.delete(report.getId());

        ResponseEntity<Report> reportResponse = restTemplate.exchange(baseUrl + "/reports/" + report.getId(),
                HttpMethod.POST, null, new ParameterizedTypeReference<>() {
                });
        Report receivedReport = reportResponse.getBody();

        assertEquals(HttpStatus.OK, reportResponse.getStatusCode(),
                "Status code for the get client by id request should be 404");

        assertNull(receivedReport);
    }
}