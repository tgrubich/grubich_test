package ru.innopolis.eventshollidays.model;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "clients")
@Getter
@Setter
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Client {
    @Id
    private UUID id;
    private String lastName;
    private String firstName;
    private String email;
    private String phone;
    private String address;
    private String passwordData;

    @OneToOne(mappedBy = "client")
    @PrimaryKeyJoinColumn
    private Event event;

    @OneToOne(mappedBy = "client")
    @PrimaryKeyJoinColumn
    private Score score;


    public Client(Client client) {
        this.id = client.getId();
        this.lastName = client.getLastName();
        this.firstName = client.getFirstName();
        this.email = client.getEmail();
        this.phone = client.getPhone();
        this.address = client.getAddress();
        this.passwordData = client.getPasswordData();
    }
}