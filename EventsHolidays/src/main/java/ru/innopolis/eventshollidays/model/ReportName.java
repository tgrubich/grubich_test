package ru.innopolis.eventshollidays.model;

public enum ReportName {
    BY_EVENTS, BY_FUNDS_RECEIVED
}
