package ru.innopolis.eventshollidays.model;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "events")
@Getter
@Setter
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Event {
    @Id
    private UUID id;

    private String dateEvent;

    private Category category;

    @OneToOne
    private Client client;

    @OneToOne
    private Employee employee;

    @OneToOne(mappedBy = "event")
    @PrimaryKeyJoinColumn
    private Score score;

    public Event(Event createdEvent) {
    }
}

