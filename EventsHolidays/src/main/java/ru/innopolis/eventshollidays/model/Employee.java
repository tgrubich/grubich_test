package ru.innopolis.eventshollidays.model;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "employee")
@Getter
@Setter
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    @Id
    private UUID id;
    private String lastName;
    private String firstName;
    private String position;
    private String phone;

    @OneToOne(mappedBy = "employee")
    @PrimaryKeyJoinColumn
    private Event event;

    @OneToOne(mappedBy = "employee")
    @PrimaryKeyJoinColumn
    private Score score;

    public Employee(Employee employee) {
        this.id = employee.getId();
        this.lastName = employee.getLastName();
        this.firstName = employee.getFirstName();
        this.position = employee.getPosition();
        this.phone = employee.getPhone();
    }
}


