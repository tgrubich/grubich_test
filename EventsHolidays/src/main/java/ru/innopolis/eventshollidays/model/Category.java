package ru.innopolis.eventshollidays.model;

public enum Category {
    FOR_ADULTS, FOR_CHILDREN
}
