package ru.innopolis.eventshollidays.model;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "scores")
@Getter
@Setter
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Score {
    @Id
    private UUID id;

    private String dateCreateScore;

    private Integer numberScore;

    private Integer price;

    @OneToOne
    private Event event;

    @OneToOne
    private Client client;

    @OneToOne
    private Employee employee;

    public Score(Score createdScore) {
    }
}
