package ru.innopolis.eventshollidays.model;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "reports")
@Getter
@Setter
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Report {
    @Id
    private UUID id;

    private String dateCreateReport;

    private ReportName reportName;
    private String reportData;

    public Report(Report createdReport) {
    }
}

