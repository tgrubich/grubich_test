package ru.innopolis.eventshollidays.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.eventshollidays.model.Event;
import ru.innopolis.eventshollidays.service.EventServices;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/events")
@RequiredArgsConstructor
public class EventController {

    private final EventServices eventServices;

    @GetMapping
    public List<Event> getAllEvents() {
        return eventServices.getAll();
    }

    @GetMapping("/{id}")
    public Event getById(@PathVariable UUID id) {
        return eventServices.getById(id);
    }

    @PostMapping
    public Event createEvent(@RequestBody Event event) {
        return eventServices.createEvent(event);
    }

    @PutMapping("/{id}")
    public Event updateEvent(@PathVariable UUID id, @RequestBody Event event) {
        return eventServices.updateEvent(id, event);
    }

    @DeleteMapping("/{id}")
    public void deleteEvent(@PathVariable UUID id) {
        eventServices.delete(id);
    }
}
