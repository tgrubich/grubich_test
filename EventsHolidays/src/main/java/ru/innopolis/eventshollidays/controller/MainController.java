package ru.innopolis.eventshollidays.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/ui/home")
@RequiredArgsConstructor
public class MainController {

    @GetMapping
    public String startPage() {
        return "home/home";
    }
}