package ru.innopolis.eventshollidays.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.eventshollidays.model.Report;
import ru.innopolis.eventshollidays.service.ReportServices;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/ui/reports")
@RequiredArgsConstructor
public class ReportsFrontendController {

    private final ReportServices reportServices;

    @GetMapping
    public String reportsPage(Model model) {
        List<Report> reports = reportServices.getAll();
        model.addAttribute("reports", reports);
        return "reports/reports";
    }

    @GetMapping("/{id}")
    public String reportPage(Model model, @PathVariable UUID id) {
        Report report = reportServices.getById(id);
        model.addAttribute("report", report);
        return "reports/report";
    }

    @GetMapping("/new")
    public String reportCreatePage(Model model) {
        model.addAttribute("report", new Report());
        return "reports/report";
    }

    @PostMapping("/delete/{id}")
    public String deleteReport(@PathVariable UUID id) {
        reportServices.delete(id);
        return "redirect:/ui/reports";
    }

    @PostMapping
    public String createOrUpdateReport(@ModelAttribute("reportDto") Report reportDto) {
        if (reportDto.getId() == null) {
            reportServices.createReport(reportDto);
        } else {
            reportServices.updateReport(reportDto.getId(), reportDto);
        }
        return "redirect:/ui/reports";
    }
}
