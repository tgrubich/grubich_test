package ru.innopolis.eventshollidays.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.eventshollidays.model.Score;
import ru.innopolis.eventshollidays.service.ScoreServices;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/scores")
@RequiredArgsConstructor
public class ScoreController {

    private final ScoreServices scoreServices;

    @GetMapping
    public List<Score> getAllScores() {
        return scoreServices.getAll();
    }

    @GetMapping("/{id}")
    public Score getById(@PathVariable UUID id) {
        return scoreServices.getById(id);
    }

    @PostMapping
    public Score createScore(@RequestBody Score score) {
        return scoreServices.createScore(score);
    }

    @PutMapping("/{id}")
    public Score updateScore(@PathVariable UUID id, @RequestBody Score score) {
        return scoreServices.updateScore(id, score);
    }

    @DeleteMapping("/{id}")
    public void deleteScore(@PathVariable UUID id) {
        scoreServices.delete(id);
    }
}
