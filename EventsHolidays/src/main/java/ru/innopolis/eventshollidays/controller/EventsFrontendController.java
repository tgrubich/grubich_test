package ru.innopolis.eventshollidays.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.eventshollidays.model.Client;
import ru.innopolis.eventshollidays.model.Employee;
import ru.innopolis.eventshollidays.model.Event;
import ru.innopolis.eventshollidays.service.ClientServices;
import ru.innopolis.eventshollidays.service.EmployeeServices;
import ru.innopolis.eventshollidays.service.EventServices;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/ui/events")
@RequiredArgsConstructor
public class EventsFrontendController {

    private final EventServices eventServices;
    private final ClientServices clientServices;
    private final EmployeeServices employeeServices;

    @GetMapping
    public String eventsPage(Model model) {
        List<Event> events = eventServices.getAll();
        model.addAttribute("events", events);
        return "events/events";
    }

    @GetMapping("/{id}")
    public String eventPage(Model model, @PathVariable UUID id) {
        Event event = eventServices.getById(id);
        model.addAttribute("event", event);
        return "events/event";
    }

    @GetMapping("/new")
    public String eventCreatePage(Model model) {
        List<Employee> employees = employeeServices.getAll();
        List<Client> clients = clientServices.getAll();
        model.addAttribute("clients", clients);
        model.addAttribute("employees", employees);
        model.addAttribute("event", new Event());
        return "events/event";
    }

    @PostMapping("/delete/{id}")
    public String deleteEvent(@PathVariable UUID id) {
        eventServices.delete(id);
        return "redirect:/ui/events";
    }

    @PostMapping
    public String createOrUpdateEvent(@ModelAttribute("eventDto") Event eventDto) {
        if (eventDto.getId() == null) {
            eventServices.createEvent(eventDto);
        } else {
            eventServices.updateEvent(eventDto.getId(), eventDto);
        }
        return "redirect:/ui/events";
    }
}
