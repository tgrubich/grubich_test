package ru.innopolis.eventshollidays.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.eventshollidays.model.Employee;
import ru.innopolis.eventshollidays.service.EmployeeServices;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/ui/employees")
@RequiredArgsConstructor
public class EmployeesFrontendController {

    private final EmployeeServices employeeServices;

    @GetMapping
    public String employeesPage(Model model) {
        List<Employee> employees = employeeServices.getAll();
        model.addAttribute("employees", employees);
        return "employees/employees";
    }

    @GetMapping("/{id}")
    public String employeePage(Model model, @PathVariable UUID id) {
        Employee employee = employeeServices.getById(id);
        model.addAttribute("employee", employee);
        return "employees/employee";
    }

    @GetMapping("/new")
    public String employeeCreatePage(Model model) {
        model.addAttribute("employee", new Employee());
        return "employees/employee";
    }

    @PostMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable UUID id) {
        employeeServices.delete(id);
        return "redirect:/ui/employees";
    }

    @PostMapping
    public String createOrUpdateEmployee(@ModelAttribute("employeeDto") Employee employeeDto) {
        if (employeeDto.getId() == null) {
            employeeServices.createEmployee(employeeDto);
        } else {
            employeeServices.updateEmployee(employeeDto.getId(), employeeDto);
        }
        return "redirect:/ui/employees";
    }
}
