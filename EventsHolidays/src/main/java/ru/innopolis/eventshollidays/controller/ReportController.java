package ru.innopolis.eventshollidays.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.eventshollidays.model.Report;
import ru.innopolis.eventshollidays.service.ReportServices;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/reports")
@RequiredArgsConstructor
public class ReportController {

    private final ReportServices reportServices;

    @GetMapping
    public List<Report> getAllReports() {
        return reportServices.getAll();
    }

    @GetMapping("/{id}")
    public Report getById(@PathVariable UUID id) {
        return reportServices.getById(id);
    }

    @PostMapping
    public Report createReport(@RequestBody Report report) {
        return reportServices.createReport(report);
    }

    @PutMapping("/{id}")
    public Report updateReport(@PathVariable UUID id, @RequestBody Report report) {
        return reportServices.updateReport(id, report);
    }

    @DeleteMapping("/{id}")
    public void deleteReport(@PathVariable UUID id) {
        reportServices.delete(id);
    }
}
