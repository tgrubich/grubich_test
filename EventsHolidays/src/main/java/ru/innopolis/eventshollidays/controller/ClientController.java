package ru.innopolis.eventshollidays.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.eventshollidays.model.Client;
import ru.innopolis.eventshollidays.service.ClientServices;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/clients")
@RequiredArgsConstructor
public class ClientController {

    private final ClientServices clientServices;

    @GetMapping
    public List<Client> getAllClients() {
        return clientServices.getAll();
    }

    @GetMapping("/{id}")
    public Client getById(@PathVariable UUID id) {
        return clientServices.getById(id);
    }

    @PostMapping
    public Client createClient(@RequestBody Client client) {
        return clientServices.createClient(client);
    }

    @PutMapping("/{id}")
    public Client updateClient(@PathVariable UUID id, @RequestBody Client client) {
        return clientServices.updateClient(id, client);
    }

    @DeleteMapping("/{id}")
    public void deleteClient(@PathVariable UUID id) {
        clientServices.delete(id);
    }
}
