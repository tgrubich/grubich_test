package ru.innopolis.eventshollidays.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.eventshollidays.model.Client;
import ru.innopolis.eventshollidays.model.Employee;
import ru.innopolis.eventshollidays.model.Event;
import ru.innopolis.eventshollidays.model.Score;
import ru.innopolis.eventshollidays.service.ClientServices;
import ru.innopolis.eventshollidays.service.EmployeeServices;
import ru.innopolis.eventshollidays.service.EventServices;
import ru.innopolis.eventshollidays.service.ScoreServices;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/ui/scores")
@RequiredArgsConstructor
public class ScoresFrontendController {

    private final ScoreServices scoreServices;
    private final EventServices eventServices;
    private final ClientServices clientServices;
    private final EmployeeServices employeeServices;

    @GetMapping
    public String scoresPage(Model model) {
        List<Score> scores = scoreServices.getAll();
        model.addAttribute("scores", scores);
        return "scores/scores";
    }

    @GetMapping("/{id}")
    public String scorePage(Model model, @PathVariable UUID id) {
        Score score = scoreServices.getById(id);
        model.addAttribute("score", score);
        return "scores/score";
    }

    @GetMapping("/new")
    public String scoreCreatePage(Model model) {
        List<Employee> employees = employeeServices.getAll();
        List<Client> clients = clientServices.getAll();
        List<Event> events = eventServices.getAll();
        model.addAttribute("employees", employees);
        model.addAttribute("clients", clients);
        model.addAttribute("events", events);
        model.addAttribute("score", new Score());
        return "scores/score";
    }

    @PostMapping("/delete/{id}")
    public String deleteScore(@PathVariable UUID id) {
        scoreServices.delete(id);
        return "redirect:/ui/scores";
    }

    @PostMapping
    public String createOrUpdateScore(@ModelAttribute("scoreDto") Score scoreDto) {
        if (scoreDto.getId() == null) {
            scoreServices.createScore(scoreDto);
        } else {
            scoreServices.updateScore(scoreDto.getId(), scoreDto);
        }
        return "redirect:/ui/scores";
    }
}
