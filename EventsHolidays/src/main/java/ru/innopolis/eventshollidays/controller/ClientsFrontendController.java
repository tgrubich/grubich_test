package ru.innopolis.eventshollidays.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.eventshollidays.model.Client;
import ru.innopolis.eventshollidays.service.ClientServices;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/ui/clients")
@RequiredArgsConstructor
public class ClientsFrontendController {

    private final ClientServices clientServices;

    @GetMapping
    public String clientsPage(Model model) {
        List<Client> clients = clientServices.getAll();
        model.addAttribute("clients", clients);
        return "clients/clients";
    }

    @GetMapping("/{id}")
    public String clientPage(Model model, @PathVariable UUID id) {
        Client client = clientServices.getById(id);
        model.addAttribute("client", client);
        return "clients/client";
    }

    @GetMapping("/new")
    public String clientCreatePage(Model model) {
        model.addAttribute("client", new Client());
        return "clients/client";
    }

    @PostMapping("/delete/{id}")
    public String deleteClient(@PathVariable UUID id) {
        clientServices.delete(id);
        return "redirect:/ui/clients";
    }

    @PostMapping
    public String createOrUpdateClient(@ModelAttribute("clientDto") Client clientDto) {
        if (clientDto.getId() == null) {
            clientServices.createClient(clientDto);
        } else {
            clientServices.updateClient(clientDto.getId(), clientDto);
        }
        return "redirect:/ui/clients";
    }
}
