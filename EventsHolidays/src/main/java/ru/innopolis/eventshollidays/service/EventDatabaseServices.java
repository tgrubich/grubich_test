package ru.innopolis.eventshollidays.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.innopolis.eventshollidays.model.Event;
import ru.innopolis.eventshollidays.repository.ClientRepository;
import ru.innopolis.eventshollidays.repository.EmployeeRepository;
import ru.innopolis.eventshollidays.repository.EventRepository;

import java.util.*;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class EventDatabaseServices implements EventServices {

    private final EventRepository eventRepository;

    @Override
    public Event getById(UUID id) {
        return eventRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Event with id %s not found", id)));
    }

    @Override
    public List<Event> getAll() {
        Iterable<Event> eventIterable = eventRepository.findAll();
        List<Event> eventList = new ArrayList<>();
        eventIterable.forEach(eventList::add);
        return eventList;
    }

    @Override
    public Event createEvent(Event event) {
        event.setId(UUID.randomUUID());
        return eventRepository.save(event);
    }

    @Override
    public Event updateEvent(UUID id, Event event) {
        Event eventFromDatabase = getById(id);

        Event eventToUpdate = eventFromDatabase.toBuilder()
                .dateEvent(event.getDateEvent())
                .category(event.getCategory())
                .client(event.getClient())
                .employee(event.getEmployee())
                .build();

        return eventRepository.save(eventToUpdate);
    }

    @Override
    public void delete(UUID id) {
        eventRepository.deleteById(id);
    }
}
