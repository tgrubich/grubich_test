package ru.innopolis.eventshollidays.service;

import ru.innopolis.eventshollidays.model.Event;

import java.util.List;
import java.util.UUID;

public interface EventServices {
    Event getById(UUID id);

    List<Event> getAll();

    Event createEvent(Event event);

    Event updateEvent(UUID id, Event event);

    void delete(UUID id);
}
