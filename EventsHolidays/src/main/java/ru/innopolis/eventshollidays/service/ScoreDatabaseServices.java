package ru.innopolis.eventshollidays.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.innopolis.eventshollidays.model.Score;
import ru.innopolis.eventshollidays.repository.ScoreRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class ScoreDatabaseServices implements ScoreServices {

    private final ScoreRepository scoreRepository;

    @Override
    public Score getById(UUID id) {
        return scoreRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Score with id %s not found", id)));
    }

    @Override
    public List<Score> getAll() {
        Iterable<Score> scoreIterable = scoreRepository.findAll();
        List<Score> scoreList = new ArrayList<>();
        scoreIterable.forEach(scoreList::add);
        return scoreList;
    }

    @Override
    public Score createScore(Score score) {
        score.setId(UUID.randomUUID());
        return scoreRepository.save(score);
    }

    @Override
    public Score updateScore(UUID id, Score score) {
        Score scoreFromDatabase = getById(id);

        Score scoreToUpdate = scoreFromDatabase.toBuilder()
                .dateCreateScore(score.getDateCreateScore())
                .numberScore(score.getNumberScore())
                .event(score.getEvent())
                .client(score.getClient())
                .employee(score.getEmployee())
                .price(score.getPrice())
                .build();

        return scoreRepository.save(scoreToUpdate);
    }

    @Override
    public void delete(UUID id) {
        scoreRepository.deleteById(id);
    }
}
