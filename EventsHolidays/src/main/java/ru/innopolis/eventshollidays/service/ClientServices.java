package ru.innopolis.eventshollidays.service;

import ru.innopolis.eventshollidays.model.Client;

import java.util.List;
import java.util.UUID;

public interface ClientServices {

    Client getById(UUID id);

    List<Client> getAll();

    Client createClient(Client client);

    Client updateClient(UUID id, Client client);

    void delete(UUID id);
}
