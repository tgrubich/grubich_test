package ru.innopolis.eventshollidays.service;

import ru.innopolis.eventshollidays.model.Score;

import java.util.List;
import java.util.UUID;

public interface ScoreServices {

    Score getById(UUID id);

    List<Score> getAll();

    Score createScore(Score score);

    Score updateScore(UUID id, Score score);

    void delete(UUID id);
}
