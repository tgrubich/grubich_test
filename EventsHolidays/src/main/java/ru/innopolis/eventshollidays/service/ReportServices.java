package ru.innopolis.eventshollidays.service;

import ru.innopolis.eventshollidays.model.Report;

import java.util.List;
import java.util.UUID;

public interface ReportServices {
    Report getById(UUID id);

    List<Report> getAll();

    Report createReport(Report report);

    Report updateReport(UUID id, Report report);

    void delete(UUID id);
}
