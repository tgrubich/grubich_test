package ru.innopolis.eventshollidays.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.innopolis.eventshollidays.model.Client;
import ru.innopolis.eventshollidays.repository.ClientRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class ClientDatabaseServices implements ClientServices {

    private final ClientRepository clientRepository;

    @PostConstruct
    public void init() {
        long clientsCount = clientRepository.count();

        if (clientsCount < 3) {
            for (int i = 1; i <= 3; i++) {
                Client client = Client.builder()
                        .id(UUID.randomUUID())
                        .lastName("Markoff")
                        .firstName("Martin")
                        .email("martin.page@example.com")
                        .phone("+79605556677")
                        .address("Kazan")
                        .passwordData("1234abcd")
                        .build();
                clientRepository.save(client);
            }
        }
    }

    @Override
    public Client getById(UUID id) {
        return clientRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Client with id %s not found", id)));
    }

    @Override
    public List<Client> getAll() {
        Iterable<Client> clientIterable = clientRepository.findAll();
        List<Client> clientList = new ArrayList<>();
        clientIterable.forEach(clientList::add);
        return clientList;

    }

    @Override
    public Client createClient(Client client) {
        client.setId(UUID.randomUUID());
        return clientRepository.save(client);
    }

    @Override
    public Client updateClient(UUID id, Client client) {
        Client clientFromDatabase = getById(id);

        Client clientToUpdate = clientFromDatabase.toBuilder()
                .lastName(client.getLastName())
                .firstName(client.getFirstName())
                .email(client.getEmail())
                .phone(client.getPhone())
                .address(client.getAddress())
                .passwordData(client.getPasswordData())
                .build();

        return clientRepository.save(clientToUpdate);
    }

    @Override
    public void delete(UUID id) {
        clientRepository.deleteById(id);
    }
}
