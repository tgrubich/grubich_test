package ru.innopolis.eventshollidays.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.innopolis.eventshollidays.model.Report;
import ru.innopolis.eventshollidays.model.ReportName;
import ru.innopolis.eventshollidays.repository.ReportRepository;

import javax.annotation.PostConstruct;
import java.util.*;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class ReportDatabaseServices implements ReportServices {

    private final ReportRepository reportRepository;

    @PostConstruct
    public void init() {
        long reportsCount = reportRepository.count();
        Calendar calendar = new GregorianCalendar(2021, 11, 8);
        calendar.add(Calendar.DATE, 1);
        Date date = calendar.getTime();

        if (reportsCount < 10) {
            for (int i = 1; i <= 10; i++) {
                Report report = Report.builder()
                        .id(UUID.randomUUID())
                        .dateCreateReport("11.12.2021")
                        .reportName(ReportName.BY_EVENTS)
                        .reportData("Report data " + i)
                        .build();
                reportRepository.save(report);
            }
        }
    }

    @Override
    public Report getById(UUID id) {
        return reportRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Report with id %s not found", id)));
    }

    @Override
    public List<Report> getAll() {
        Iterable<Report> reportIterable = reportRepository.findAll();
        List<Report> reportList = new ArrayList<>();
        reportIterable.forEach(reportList::add);
        return reportList;
    }

    @Override
    public Report createReport(Report report) {
        report.setId(UUID.randomUUID());
        return reportRepository.save(report);
    }

    @Override
    public Report updateReport(UUID id, Report report) {
        Report reportFromDatabase = getById(id);

        Report reportToUpdate = reportFromDatabase.toBuilder()
                .dateCreateReport(report.getDateCreateReport())
                .reportName(report.getReportName())
                .reportData(report.getReportData())
                .build();

        return reportRepository.save(reportToUpdate);
    }

    @Override
    public void delete(UUID id) {
        reportRepository.deleteById(id);
    }
}
