package ru.innopolis.eventshollidays.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.innopolis.eventshollidays.model.Employee;
import ru.innopolis.eventshollidays.repository.EmployeeRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class EmployeeDatabaseServices implements EmployeeServices {

    private final EmployeeRepository employeeRepository;

    @PostConstruct
    public void init() {
        long employeesCount = employeeRepository.count();

        if (employeesCount < 3) {
            for (int i = 1; i <= 3; i++) {
                Employee employee = Employee.builder()
                        .id(UUID.randomUUID())
                        .lastName("Lucic")
                        .firstName("Andrey")
                        .position("manager")
                        .phone("+79608889900")
                        .build();
                employeeRepository.save(employee);
            }
        }
    }

    @Override
    public Employee getById(UUID id) {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Employee with id %s not found", id)));
    }

    @Override
    public List<Employee> getAll() {
        Iterable<Employee> employeeIterable = employeeRepository.findAll();
        List<Employee> employeeList = new ArrayList<>();
        employeeIterable.forEach(employeeList::add);
        return employeeList;
    }

    @Override
    public Employee createEmployee(Employee employee) {
        employee.setId(UUID.randomUUID());
        return employeeRepository.save(employee);
    }

    @Override
    public Employee updateEmployee(UUID id, Employee employee) {
        Employee employeeFromDatabase = getById(id);

        Employee employeeToUpdate = employeeFromDatabase.toBuilder()
                .lastName(employee.getLastName())
                .firstName(employee.getFirstName())
                .position(employee.getPosition())
                .phone(employee.getPhone())
                .build();

        return employeeRepository.save(employeeToUpdate);
    }

    @Override
    public void delete(UUID id) {
        employeeRepository.deleteById(id);
    }
}
