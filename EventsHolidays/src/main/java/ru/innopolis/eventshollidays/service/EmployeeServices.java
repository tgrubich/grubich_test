package ru.innopolis.eventshollidays.service;

import ru.innopolis.eventshollidays.model.Employee;

import java.util.List;
import java.util.UUID;

public interface EmployeeServices {
    Employee getById(UUID id);

    List<Employee> getAll();

    Employee createEmployee(Employee employee);

    Employee updateEmployee(UUID id, Employee employee);

    void delete(UUID id);
}
