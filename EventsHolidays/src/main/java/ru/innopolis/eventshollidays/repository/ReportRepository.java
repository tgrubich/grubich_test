package ru.innopolis.eventshollidays.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.innopolis.eventshollidays.model.Report;

import java.util.UUID;

@Repository
public interface ReportRepository extends CrudRepository<Report, UUID> {
}
