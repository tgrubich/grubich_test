package ru.innopolis.eventshollidays.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.innopolis.eventshollidays.model.Score;

import java.util.UUID;

@Repository
public interface ScoreRepository extends CrudRepository<Score, UUID> {
}
