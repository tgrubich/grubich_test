CREATE TABLE employee (
        id UUID PRIMARY KEY,
        lastName varchar(255),
        firstName varchar(255),
        position varchar(255),
        phone varchar(255),
)