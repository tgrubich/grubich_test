CREATE TABLE clients
(
    id           UUID PRIMARY KEY,
    lastName     varchar(255),
    firstName    varchar(255),
    email        varchar(255),
    phone        varchar(255),
    address      varchar(255),
    passwordData varchar(255)
);

