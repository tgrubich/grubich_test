CREATE TABLE scores (
                         id UUID PRIMARY KEY,
                         dateCreateScore date,
                         numberScore INTEGER,
                         eventName varchar(255),
                         client varchar(255),
                         employee varchar(255),
)