CREATE TABLE reports (
                        id UUID PRIMARY KEY,
                        dateCreateReport date,
                        reportName varchar(255),
                        reportData varchar(255),
)