CREATE TABLE events (
                          id UUID PRIMARY KEY,
                          dateEvent date,
                          category varchar(255),
                          client varchar(255),
                          employee varchar(255),
)